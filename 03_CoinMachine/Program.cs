/*
 * Nyní vytvořte nový soubor "CoinHelpers.cs"
 * Vpravo v okně "Solution Explorer" (Průzkumník řešení) kliknětě pravým tlačítkem na projekt "03_CoinMachine".
 * Vyberte submenu "Add" (Přidat)
 * A poté "New Item" (Nový soubor?)
 * 
 * Poté upravte obsah podobně jako CampaignHelpers nebo MathHelpers.
 * Tedy: "public static class CoinHelpers"
 * 
 * Poté uvnitř vytvořte metodu, která bude mít dva vstupní parametry
 *   Obnos
 *   Mince
 * A jeden výstupní parametr
 *   ZbyvajiciObnos
 * Metoda poté vypíše na obrazovku, kolik mincí je v dané hodnotě a vrátí zbývající obnos již bez mincí vstupní hodnoty.
 * Příklad:
 *   (obnos: 399, mince: 50) -> vypíši 7x50 na obrazvku a vrátím 49 (399 - 7x50)
 */

int amount = 399;
Console.WriteLine(amount);

/*
 * Odkomentujte následující dva řádky a doplňte volání Vaší nové metody.
 */

//amount = /*zde zavolejte Vaši novou metodu pro mince hodnoty 50 */;
//Console.WriteLine(amount);

/*
 * S pomocí Vaší metody vyřešte ostatní hodnoty mincí (20, 10, 5, 2, 1) 
 */


/*
 * Upravte metodu tak, aby na obrazovku vypisovala jen v případě, že počet mincí v obnose je větší jak 0.
 * Smažte přebytečná vypisování na obrazovku.
 * Tedy aby program po spuštění vypsal
 *   399
 *   Dostanete 7x50
 *   Dostanete 2x20
 *   Dostanete 1x5
 *   Dostanete 2x2
 */


/*
 * Přijdou Vám metody užitečné nebo zbytečné? A proč?
 * Než odejdete, zkuste ještě párkrát změnit vstupní obnos a ověřit funkčnost.
 */


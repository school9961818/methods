using _02_ReturnValueMethods;

int a = 2;
int b = 3;

int result = MathHelpers.GetSummaryOfNumbers(a, b);

Console.WriteLine(result);

/*
 * Další typ metody, který záhy přišel, je metoda s výstupním parametrem.
 * Používá se, když chceme z metody vracet nějakou hodnotu (např: vypočítej výplatu, vrať optimální rozměr, atd...)
 * Zkuste program spustit a poté se podívat na definici metody "GetSummaryOfNumbers" (F12)
 */


/*
 * Nyní zkuste napsat vlastní metodu (do MathHelpers.cs), která vrátí součin dvou čísel.
 * Zavolejte ji a výsledek vypište na obrazovku.
 */


/*
 * Zbytku po celočíselném dělení se říká modulo.
 * Získáme ho operátorem "%"
 * Odkomentujte následující kód a zkuste.
 */

//int moduloResult = 7 % 3;
//Console.WriteLine($"Modulo (remainder) after: 7 / 3 = {moduloResult}");

//int divisionResult = 7 / 3;
//Console.WriteLine($"Division (whole numbers) after: 7 / 3 = {divisionResult}");

/*
 * Sudá čísla.
 * Napište metodu, která bude mít vstupní parametr
 *   číslo
 * A vrátí bool (true nebo false) podle toho, jestli je číslo sudé.
 * (pozn. sudá čísla mají zbytek po celočíselném dělení dvojkou roven nule)
 * Zavolejte ji aslespoň dvakrát (pro sudé a liché číslo) a výsledek vypisujte na obrazovku
 */


/*
 * Lichá čísla.
 * Napište metodu, která bude mít vstupní parametr
 *   číslo
 * A vrátí bool (true nebo false) podle toho, jestli je číslo liché.
 * Zavolejte ji aslespoň dvakrát (pro sudé a liché číslo) a výsledek vypisujte na obrazovku
 *   Pozn. Co se stane, když znegujete předchozí metodu? Pokud číslo není sudé, je jaké?
 */

/*
 * Napište metodu, která bude mít vstupní parametry
 *   číslo
 *   dělitel
 * A vrátí bool podle toho, jestli je číslo dělitelné beze zbytku dělitelem.
 * Příklad
 *   (číslo: 8, dělitel: 2) -> true
 *   (číslo: 9, dělitel: 3) -> true
 *   (číslo: 9, dělitel: 2) -> false
 *   
 * Metodu několikrát zavolejte s různými parametry a výsledek vypisujte na obrazovku
 */

/*
 * Napište metodu (do MathHelpers.cs), která bude mít dva vstupní parametry: 
 *   cena 
 *   daň
 * Vrátí cenu s daní (cena: 200, dan: 22% -> cena s daní: 244)
 * Zavolejte ji a výsledek vypište na obrazovku.
 * 
 * Zkuste metodu zavolat s parametry
 *   cena: 9
 *   daň: 10
 * Jaký je výsledek?
 * 
 * Budeme potřebovat formát s desetinnými čísly. Vygooglete, který z následujících datových typů bude nejvhodnější použít (a proč)
 *   Double
 *   Float
 *   Decimal
 * Upravte návratový datový typ metody tak, aby vrátila cenu "9.9"
 */


/*
 *  Napište metodu (do MathHelpers.cs) s vhodnými datovými typy.
 *  Vstupní parametry
 *    Cena s daní
 *    Daň v procentech
 *  Výstupní parametr
 *    Cena bez daně 
 *  (Např: (122, 22%) -> 100)
 */

/*
 * BONUS:
 * Napište metodu, která rozhodne, jestli je číslo prvočíslo.
 * Prvočíslo je každé číslo, které lze beze zbytku vydělit pouze jedničkou nebo číslem samotným (např. 7, 11, 13,...)
 * Zkuste využít již existujících metod (metodu můžeme volat uvnitř jiné metody)
 */


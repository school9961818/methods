﻿namespace _02_ReturnValueMethods
{
    public static class MathHelpers
    {
        /*
         * Typ výstupního parametru definujeme hned po "public static".
         * V tomto případě metoda vrací celé číslo (int).
         * V předchozím cvičení jsme používali "void", což znamená, že metoda nic nevrací.
         * Oproti vstupním parametrům je výstupní parametr vždy PRÁVĚ JEDEN.
         * Pokud má metoda výstupní parametr, musí obsahovat příkaz "return", který metodu ukončí a vrátí danou hodnotu.
         * 
         * Přidejte nový vstupní parametr "int z" a upravte metodu, aby vracela součet všech tří čísel.
         * Vraťte se do "Program.cs" a upravte volání.
         */
        public static int GetSummaryOfNumbers(int x, int y)
        {
            int result = x + y;
            return result;
        }
    }
}

## Prerequisites

[Cycles](https://gitlab.com/school9961818/cycles)

## Goal

* Code
  * Methods
  * Real numbers
* Visual studio
  * Multiple files
  
## Diffuculty

* Beginners
* 3-5x45min 

## Instructions

Tři základní věci v programování jsou
* **Proměnné**
* **Podmínky**
* **Cykly**

Neustále se opakují a neustále se používají. Libovolně složitý program je tvořen právě z podmínek, cyklů a proměnných. Pokud se je naučíte efektivně používat, můžete naprogramovat cokoliv. 

### To je programování tak snadné?
Bohužel ne. Proměnné, podmínky a cykly sice tvoří základ, chcete-li _gramatiku_, ale pořád jsou tu ješte příkazy (_slovní zásoba_), nástroje (Visual Studio, GitLab,...), procesní a technická dokumentace (programování je týmová záležitost) nebo software architektura    (jak zapadá můj program do větší skládačky).

Každá s těchto domén se navíc neustále (a rychle) vyvíjí, takže by bylo fér říct už na začátku: **programátorem se člověk nestane, ale udržuje**. Podobně jako když se chcete udržet v kondici; není to jednorázová akce, ale trvalá nekončící snaha. Programování je krásná a kreativní, ale náročná a neustálé sebevzdělávání vyžadující profese.

### Metody
Když s proměnnými, podmínkami  a cykly dokážeme naprogramovat cokoliv, proč potřebujeme něco dalšího? Odpověď je složitost. Jak se počítače zrychlovaly a operační paměti zvětšovaly, rostla i složitost a s tím délka programů/aplikací. Lidé se přestávali orientovat v kódu, který byl třeba 100 000 řádků dlouhý. A tak vznikly **metody** (jiným názvem také podprogramy, funkce, procedury, subrutiny,...)

Primární benefit metod je opakované používání kódu. Abychom stejnou věc nemuseli dělat dvakrát. Metodu nadefinujeme jednou a můžeme ji použít opakovaně

```mermaid
sequenceDiagram
	MainCode->>MainCode: Do stuff
	create  participant  Method1  as  Method
	MainCode->>Method1: Call a method
	Method1->>Method1: Run code inside method
	destroy  Method1
	Method1->>MainCode: Return to main code
	MainCode->>MainCode: Do more stuff
	create  participant  Method2  as  Method
	MainCode->>Method2: Call method again
	Method2->>Method2: Run code inside method
	destroy  Method2
	Method2->>MainCode: Return to main code
```

Naklonujte si projekt do počítače a postupně vyřešte zadání ve všech projektech.
 
## Feedback

https://forms.gle/mkYXVbhQvNLjucDG8

## Where to next

[Structures](https://gitlab.com/school9961818/structures)

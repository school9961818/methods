﻿Console.WriteLine("Main code");
MyFirstMethod();
Console.WriteLine("More main code");
MyFirstMethod();

void MyFirstMethod()
{
    Console.WriteLine("  method");
}

/*
 * Zkuste si program spustit. 
 * Máme zde vytvořenou jednoduchou metodu "MyFirstMethod", kterou kdykoliv zavoláme, tak se provede.
 * Přesněji provede se její tělo, což je veškerý kód mezi složenými závorkami. 
 * Zkuste do výpisu v metodě přidat vykřičník ("  method!") a spustit.
 * Vidíte, úprava na jednom místě se projevila dvakrát!.
 * Zkuste metodu zavolat ještě jednou. Úplně na začátku programu.
 * 
 * Takhle může vypadat jednoduchá metoda.
 * Stejně jako proměnné musí mít metody unikátní název.
 * Rozdíl mezi proměnnou a voláním metody poznáte podle kulatých závorek (ty má metoda vždy).
 * 
 * Trochu dvousečná zbraň je, že metody můžeme definovat kdekoliv.
 * Zkuste celou metodu "MyFirstMethod" (i s tělem) přesunout na začátek kódu. Funguje?
 * Zkuste celou metodu "MyFirstMethod" (i s tělem) přesunout někam doprostřed kódu. Funguje?
 * A je to přehledné?
 * 
 * Nové pravidlo tohoto kurzu: 
 *        === žádná metoda nebude uvnitř Program.cs ===
 *        
 * Nyní můžete celou "MyFirstMethod" smazat (včetně volání).
 * Na pravé straně v okně "SolutionExplorer" uvidíte soubor "CampaignHelpers.cs".
 * Dvojklikem si ho otevřete a přesuňtě se tam.
 */









/*
CampaignHelpers.DisplayCustomCard("Jane", "promotion");
CampaignHelpers.DisplayCustomCard("Ichigo", "bankai");
*/
/*
  Pozn: po odkomentování volání bude "CampaignHelpers" červeně podtrženo.
        Visual studio Vám chce vždy pomoci. Když kliknete na kód podtržený červeně,
        uvidíte vlevo žlutou žárovku (nebo můžete zmáčknout "Ctrl + .").
        Nabídne se Vám několik možností a zpravidla bývá ta první správná.
        Vyberte z nabídky "Using Methods;" a klikněte (nebo Enter)
*/

/*
 * Díky vstupním parametrům můžeme i ze stejné metody dostat různé výsledky.
 * Přidejte ještě jedno volání "DisplayCustomCard" a popřejte k něčemu kamarádovi.
 */

/*
 * Máte? Nyní si zkusíme metodu vylepšit.
 * Upravte definici metody (v souboru CampaignHelpers.cs) a přidejte na konec nový vstupní parametr (string signature).
 * V tomto parametru bude očekávaný podpis - tímto podpisem nahraďte "truly".
 *   Příklad: Pokud v novém parametru bude hodnota "Felix", pak na vypsané kartě bude "...Yours Felix."
 * Jakmile metodu upravíte, vraťte se sem.
 * Program Vám nepůjde spustit, protože metoda najednou očekává 3 vstupní parametry.
 * Vytvořte nad prvním voláním metody proměnnou "string name", kam uložíte Váš podpis.
 * Tuto proměnnou přidejte jako třetí vstupní parametr do všech volání metody.
 * Zkuste spustit.
 * 
 * Závěr: Pokud metoda očekává vstupní parametry, musí nějaké hodnoty dostat.
 *        Je ale jedno, jestli tu hodnotu předáte napřímo nebo pomocí proměnné.
 */


/*
 * Uvnitř metod můžeme psát jakýkoliv kód, včetně podmínek a cyklů.
 * Zavolejte metodu "CampaignHelpers.Fundraise" (stačí začít psát, visual studio Vám napoví).
 * Zkuste spustit.
 * Poté metodu upravte (můžete na ni kliknout a dát F12 / pravý click a "Go to definition").
 * Do metody přidejte vstupní parametr "int repeatCount" a upravte metodu tak, 
 * aby varování vypsala tolikrát, kolik je hodnota v repeatCount.
 * Upravte volání a zkuste.
 */



/*
 * V souboru CampaignHelpers.cs vytvořte úplně novou metodu,
 * která na obrazovku vypíše aktuální datum a čas.
 * Budete muset vygooglit příkaz pro aktuální datum a čas (může být i UTC).
 * Také se zamyslete nad pojmenováním metody.
 * Zavolejte metodu a zkuste.
 */



/* 
 * Vytvořte v CampaignHelper.cs novou metodu "StickAndClear".
 * Metoda nebude mít žádné vstupní parametry.
 * Po zavolání metody program počká 1 sekundu a poté vyčistí příkazovou řádku.
 * Budete potřebovat dva nové příkazy, které jsme si neukazovali
 *   - jeden pro pozastavení programu na 1s
 *   - jeden pro vyčištění příkazové řádky
 * Zavolejte metodu po každém volání "DisplayCustomCard".
 * Zkuste.
 */
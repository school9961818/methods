﻿namespace Methods
{
    public static class CampaignHelpers
    {
        /*
         * To, co vidíte nahoře (namespace, public static class) si vysvětlíme později.
         * Zatím to tedy můžete ignorovat, jen pozor na závorky.
         * 
         *   === Každá levá (otevírací) závorka musí mít pravou (zavírací) závorku ===
         *                  Platí pro všechny typy závorek ({[]})
         */


        /*
         * Nyní si představíme další metodu.
         * Protože není v Program.cs, musí začít jako "public static void" (později si vysvětlíme proč)
         * Oproti první metodě má tato vstupní parametry (vedle jména uvnitř kulatých závorek)
         */
        public static void DisplayCustomCard(string recipient, string achievement)
        {
            Console.WriteLine($"Dear {recipient},");
            Console.WriteLine($"congratulations on your {achievement}!");
            Console.WriteLine("You are a rockstar.");
            Console.WriteLine("All the best,");
            Console.WriteLine("Yours truly.");
            Console.WriteLine();
        }
        /*
         * Vraťte se do Program.cs kde odkomentujte volání této metody (zkuste "Ctrl + Tab")
         */




        public static void Fundraise()
        {
            Console.WriteLine("Please support our efforts. Thank you very much.");
        }
    }
}
